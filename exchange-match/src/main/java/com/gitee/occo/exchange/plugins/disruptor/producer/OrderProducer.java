package com.gitee.occo.exchange.plugins.disruptor.producer;

import org.springframework.beans.BeanUtils;

import com.gitee.occo.exchange.plugins.disruptor.event.OrderEvent;
import com.lmax.disruptor.EventTranslatorOneArg;
import com.lmax.disruptor.RingBuffer;

public class OrderProducer {

	private final RingBuffer<OrderEvent> ringBuffer;

	public OrderProducer(RingBuffer<OrderEvent> ringBuffer) {
		this.ringBuffer = ringBuffer;
	}

	private static final EventTranslatorOneArg<OrderEvent, OrderEvent> TRANSLATOR = new EventTranslatorOneArg<OrderEvent, OrderEvent>() {
		public void translateTo(OrderEvent event, long sequence, OrderEvent input) {
			BeanUtils.copyProperties(input,event);
		}
	};
 
	public void onData(OrderEvent input) {
		ringBuffer.publishEvent(TRANSLATOR, input);
	}
}
