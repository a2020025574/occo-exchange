package com.gitee.occo.exchange.plugins.disruptor;

import java.util.concurrent.ThreadFactory;

import com.gitee.occo.exchange.plugins.disruptor.event.OrderEvent;
import com.gitee.occo.exchange.plugins.disruptor.factory.OrderEventFactory;
import com.gitee.occo.exchange.plugins.disruptor.handler.DepthInputHandler;
import com.gitee.occo.exchange.plugins.disruptor.handler.DepthOutHandler;
import com.gitee.occo.exchange.plugins.disruptor.handler.MatchHandler;
import com.gitee.occo.exchange.plugins.disruptor.producer.OrderProducer;
import com.lmax.disruptor.RingBuffer;
import com.lmax.disruptor.YieldingWaitStrategy;
import com.lmax.disruptor.dsl.Disruptor;
import com.lmax.disruptor.dsl.ProducerType;

public class DisruptorConfig {
	
	static Disruptor<OrderEvent> disruptor;
	static{
		OrderEventFactory factory = new OrderEventFactory();
		int ringBufferSize = 1024*1024;
		ThreadFactory threadFactory = runnable -> new Thread(runnable);
		disruptor = new Disruptor<>(factory, ringBufferSize, threadFactory,ProducerType.MULTI, new YieldingWaitStrategy());
		disruptor.handleEventsWithWorkerPool(new MatchHandler(),new MatchHandler()).then(new DepthInputHandler(),new DepthOutHandler());
		disruptor.start();
	}
	
	public static void producer(OrderEvent input){
		RingBuffer<OrderEvent> ringBuffer = disruptor.getRingBuffer();
		OrderProducer producer = new OrderProducer(ringBuffer);
		producer.onData(input);
	}
}
