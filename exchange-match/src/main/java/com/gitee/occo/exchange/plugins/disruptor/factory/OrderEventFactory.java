package com.gitee.occo.exchange.plugins.disruptor.factory;

import com.gitee.occo.exchange.plugins.disruptor.event.OrderEvent;
import com.lmax.disruptor.EventFactory;

/**
 * 事件生成工厂（用来初始化预分配事件对象）
 * 创建者 kinbug
 */
public class OrderEventFactory implements EventFactory<OrderEvent>{

	@Override
	public OrderEvent newInstance() {
		// TODO Auto-generated method stub
		return new OrderEvent();
	}

}
