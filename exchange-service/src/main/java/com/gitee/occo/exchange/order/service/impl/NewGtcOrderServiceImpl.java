package com.gitee.occo.exchange.order.service.impl;

import org.springframework.beans.factory.InitializingBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.gitee.occo.common.cqrs.CommandBus;
import com.gitee.occo.exchange.common.entity.Order;
import com.gitee.occo.exchange.common.enums.OrderType;
import com.gitee.occo.exchange.fund.FrozenCommand;
import com.gitee.occo.exchange.order.OrderFactory;
import com.gitee.occo.exchange.order.service.NewOrderService;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class NewGtcOrderServiceImpl implements NewOrderService, InitializingBean {
	
	@Autowired
	private FrozenCommand frozenCommand;
	
	@Override
	public Order NewOrder(Order order) {
		Boolean rsg = new CommandBus<Order>().dispatch(frozenCommand, order);
		if (rsg) {
			log.info("====入库成功！");
		}
		return order;
	}

	@Override
	public void afterPropertiesSet() throws Exception {
		// TODO Auto-generated method stub
		OrderFactory.register(OrderType.GTC, this);
	}
}
