package com.gitee.occo.exchange.fund;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitee.occo.common.cqrs.Command;
import com.gitee.occo.exchange.common.entity.Order;
import com.gitee.occo.exchange.entity.FundOperatesDto;

@Component
public class UnFrozeCommand implements Command<Order,Boolean> {

	@Autowired
	private FundOperates fundOperates;
	
	@Override
	public Boolean execute(Order param) {
		long number = param.getNumber();
		if (param.isBid()) {
			number = param.getAmount();
		}
		String coin = "BTC";
		FundOperatesDto dto = new FundOperatesDto(param.getUid(),coin, number);
		return fundOperates.unfreeze(dto);
	}
}
