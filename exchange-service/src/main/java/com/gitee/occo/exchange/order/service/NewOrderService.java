package com.gitee.occo.exchange.order.service;

import com.gitee.occo.exchange.common.entity.Order;

public interface NewOrderService {
	
	Order NewOrder(Order order);
}
