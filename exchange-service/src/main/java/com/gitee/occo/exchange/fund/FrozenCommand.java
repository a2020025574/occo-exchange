package com.gitee.occo.exchange.fund;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.gitee.occo.common.cqrs.Command;
import com.gitee.occo.exchange.common.entity.Order;
import com.gitee.occo.exchange.entity.FundOperatesDto;

@Component
public class FrozenCommand implements Command<Order,Boolean> {
	
	@Autowired
	private FundOperates fundOperates;
	
	@Override
	public Boolean execute(Order order) {
		return fundOperates.frozen(FundOperatesDto.getFundOperatesDto(order));
	}
}
