package com.gitee.occo.exchange.common.enums;

import java.util.Arrays;
import java.util.Optional;

/***
 * Order type collection
 * @author kinbug 
 */
public enum OrderType {

	GTC(0), // Good till Cancel		 	  - 正常的限价单
	MTC(1), // Mark to Cancel			  - 市价转撤销，无对手价时撤销。
	
	GTD(2),  // Good till Date		 	  - 交易者指定交易日之前有效，之后撤销
	IOC(3),  // Immediate or Cancel 	 	  - 立即成交否则取消指令
	FAK(4),  // Fill and Kill		 	  - 指定价位成交，剩余自动撤销
	FOK(5),  // Fill or Kill			 	  - 指定价位全部成交，否则自动撤销
	MTM(6),  // Mark to market		 	  - 市价转限价
	MPO(7),  // Market protection Order 	  - 市价保护单，成交到设置的保护价位置，未成交部分转为临界值得限价单。
	LCE(8),  // lceberg order 			  - 冰山单
	SLO(9),  // stop limit order 		  - 止损限价单
	SWP(10); // stop with protection order - 止损保护单
	
	private int code;
	
	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	OrderType(int code) {
		this.code = (byte) code;
	}

	public static Optional<OrderType> of(int code) {
		return Arrays.stream(values()).filter(i -> i.code == code).findFirst();
	}
}
