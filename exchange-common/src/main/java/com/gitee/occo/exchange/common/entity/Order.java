package com.gitee.occo.exchange.common.entity;

import java.io.Serializable;

import lombok.Data;

/**
 * ps: 优化①：订单排序用id，不用时间
 * 	           优化②：计算的自动不用包装类和BigDecimal，虽然节省不了多少，蚊子腿也是肉啊。而且不会有精度问题
 */
@Data
public class Order implements Serializable{

	private static final long serialVersionUID = 2600161949279286241L;

	/**
	 * -自增id
	 * -排序用id,不用时间排序. 
	 */
	private long id;
	
	/**
	 * -用户ID
	 */
	private long uid;
	
	/**
	 * -单价
	 */
	private long price;
	
	/**
	 * -数量
	 */
	private long number;
	
	/**
	 * -总额
	 */
	private long amount;
	
	/**
	 * -是否是出价单（买价）
	 */
	private boolean isBid;
	
	/**
	 * -订单类型
	 */
	private int orderType;
	
	/**
	 * -交易队
	 */
	private int tradePair;
	
	/**
	 * -状态（0未支付，1已支付，2部分撮合，3全部撮合，4已经撤销，5完成结算，6撤销结算）
	 */
    private int state;
}
